import Vue from 'vue'
import App from './App.vue'

import './assets/styles/test.css'
import './assets/styles/test-stylus.styl'
import './assets/images/bg.jpeg'
import './assets/styles/global.styl'

// 创建一个div
const root = document.createElement('div')
// 放在body下
document.body.appendChild(root)
// 将App挂在至此
new Vue({
  render: (h) => h(App)
}).$mount(root)
