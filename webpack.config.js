const path = require('path')
const HTMLPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const ExtractPlugin = require('extract-text-webpack-plugin')
//接受启动时传递的参数
const isDev = process.env.NODE_ENV === 'development'
const config = {
  target: 'web',
  //入口文件 
  entry: path.join(__dirname, 'src/index.js'),
  //出口文件
  output: {
    filename: 'bundle.[hash:8].js',
    path: path.join(__dirname, 'dist')
  },
  //定义用什么加载器加载相应文件
  module: {

    rules: [
      //vue结尾的文件用vueloader加载
      {
        test: /.vue$/,
        loader: 'vue-loader'

      }, {
        test: /.jsx$/,
        loader: 'babel-loader'

      }, {
        test: /\.css$/,
        use: [
          'style-loader', 'css-loader'
        ]
      }, {
        test: /\.(gif|jpg|jpeg|png|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1024,
              name: '[name]-aaa.[ext]'

            }

          }

        ]

      }]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: isDev ? '"development"' : '"production"'

      }
    }),
    new HTMLPlugin()
  ]
}
//定义端口 ip  错误直接显示到页面  启动时默认打开浏览器  更新数据不刷新页面
if (isDev) {
  config.module.rules.push(
    //css-loader 处理后的代码交给postcss-loader处理
    {
      test: /\.styl/,
      use: ['style-loader',
        'css-loader', {
          loader: 'postcss-loader',
          options: {
            sourceMap: true
          }
        },
        'stylus-loader'
      ]


    })

  //页面调试源码映射
  config.devtool = '#cheap-module-eval-source-map'
  config.devServer = {
    port: 8008,
    host: '127.0.0.1',
    //错误显示到页面
    overlay: {
      errors: true
    },
    open: true,
    hot: true
  },
    config.plugins.push(
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    )
} else {
  config.entry = {
    app: path.join(__dirname, 'src/index.js'),
    vendor: ['vue']
  }
  config.output.filename = '[name].[chunkhash:8].js'
  config.module.rules.push(
    {
      test: /\.styl/,
      use: ExtractPlugin.extract({
        fallback: 'style-loader',
        use: [
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
            }
          },
          'stylus-loader'
        ]
      })
    },
  )
  config.plugins.push(
    new ExtractPlugin('styles.[contentHash:8].css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'runtime'
    })
  )
}
module.exports = config