const path = require('path')
const HTMLPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const merge = require("webpack-merge")
const ExtractPlugin = require('extract-text-webpack-plugin')
const baseConfig = require("./webpack.config.base")
//接受启动时传递的参数
const isDev = process.env.NODE_ENV === 'development'

let config

const defaultPlugins = [new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: isDev ? '"development"' : '"production"'

  }
}),
new HTMLPlugin()]
const devServer = {
  port: 8008,
  host: '127.0.0.1',
  //错误显示到页面
  overlay: {
    errors: true
  },
  open: true,
  hot: true
}
//定义端口 ip  错误直接显示到页面  启动时默认打开浏览器  更新数据不刷新页面
if (isDev) {

  config = merge(baseConfig, {
    //页面调试源码映射
    devtool: '#cheap-module-eval-source-map',
    module: {
      //css-loader 处理后的代码交给postcss-loader处理
      rules: [{
        test: /\.styl/,
        use: ['vue-style-loader',
          {
            loader: 'css-loader',
            options: {
              module: true,
              localIdentName: isDev ? '[path]-[name]-[hash:base64:5]' : "[hash:base64:5]",
            }
          }, {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          'stylus-loader'
        ]
      }
      ]
    },
    devServer: devServer,
    plugins: defaultPlugins.concat([new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()])
  })
} else {
  config = merge(baseConfig, {
    entry: {
      app: path.join(__dirname, '../src/index.js'),
      vendor: ['vue']
    },
    output: { filename: '[name].[chunkhash:8].js' },
    module: {
      rules: [
        {
          test: /\.styl/,
          use: ExtractPlugin.extract({
            fallback: 'vue-style-loader',
            use: [
              'css-loader',
              {
                loader: 'postcss-loader',
                options: {
                  sourceMap: true,
                }
              },
              'stylus-loader'
            ]
          })
        }
      ]
    }, plugins: defaultPlugins.concat([new ExtractPlugin('styles.[contentHash:8].css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'runtime'
    })])
  })
}
module.exports = config