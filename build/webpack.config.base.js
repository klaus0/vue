const path = require('path')
const createVueLoaderOptions=require('./vue-loader.config.js')
//接受启动时传递的参数
const isDev = process.env.NODE_ENV === 'development'
const config = {
  target: 'web',
  //入口文件 
  entry: path.join(__dirname, '../src/index.js'),
  //出口文件
  output: {
    filename: 'bundle.[hash:8].js',
    path: path.join(__dirname, '../dist')
  },
  //定义用什么加载器加载相应文件
  module: {

    rules: [
      //vue结尾的文件用vueloader加载
      {
        test:/\.(vue|js|jsx)$/,
        loader:'eslint-loader',
        exclude:/node_modules/,
        enforce:'pre'      
      },
      {
        test: /.vue$/,
        loader: 'vue-loader',
        options:createVueLoaderOptions(isDev)

      }, {
        test: /.jsx$/,
        loader: 'babel-loader'

      }, {
        test: /.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        use: [
          'style-loader', 'css-loader'
        ]
      }, {
        test: /\.(gif|jpg|jpeg|png|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1024,
              name: 'resource/[path][name]-[hash:8].[ext]'

            }

          }

        ]

      }]
  }
}

module.exports = config