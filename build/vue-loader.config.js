const docsLoader= require.resolve('./doc-loader')

module.exports = (isDev) => {

  return {
    //去除vue中的空格
    preserveWhitepace: true,
    //从vue中抽取Css
    extractCSS:!isDev,
    cssModules:{
      //vue 的css中class的命名
      localIdentName:isDev?'[path]-[name]-[hash:base64:5]':"[hash:base64:5]",
      //使用驼峰命名
      camelCase:true
    },
   // hotReload:false //根据环境变量去自动设置
   //针对特定组件使用特定loader  可以自己编写loader方法
   loaders:{
    "docs":docsLoader
   },
  //loader处理之前
  preLoader:{},
  //loader处理之后
  postLoader:{}
  }
}